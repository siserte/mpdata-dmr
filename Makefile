DMRFLAGS 	= -I${DMR_PATH} -L${DMR_PATH} -ldmr
FLAGS  		= -O3 -Wall -fopenmp -pedantic
MPIFLAGS    = -I${MPI_PATH}/include -L${MPI_PATH}/lib -lmpi
SLURMFLAGS  = -I${SLURM_ROOT}/include -L${SLURM_ROOT}/lib -lslurm
#DLB_HOME        = /apps/PM/dlb/git/mpich
DLBFLAGS        = -I${DLB_HOME}/include -L$(DLB_HOME)/lib -ldlb_mpi -DTALP

all: clean mpdata

mpdata: mpdata3d-nb.cpp
	mpic++ $(FLAGS) $(DMRFLAGS) $(SLURMFLAGS) mpdata3d-nb.cpp -o mpdata

talp: mpdata3d-nb.cpp
	mpic++ $(FLAGS) $(DMRFLAGS) $(SLURMFLAGS) $(DLBFLAGS) mpdata3d-nb.cpp -o mpdata

clean:
	rm -f *.out *.o core.* *.err *.so tmp hostfile.txt mpdata
