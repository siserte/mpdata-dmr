#!/bin/bash

for thread in 1 3 6 12 24 48; do
	for process in 1 2 4 8 16 32; do
	    sbatch -N$process --job-name "t$thread-p$process" ./2-run_wrapper.sbatch $thread
	done
done
