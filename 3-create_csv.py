import subprocess
import csv

THREADS = [1, 3, 6, 12, 24, 48]
PROCESSES = [1, 2, 4, 8, 16, 32]
csv_file = 'scalability-results.csv'

with open(csv_file, mode='w', newline='') as file:
    writer = csv.writer(file)
    row = THREADS[:]
    row.insert(0, "Processes\Threads")
    writer.writerow(row)
    for p in PROCESSES:
        row = [p]
        for t in THREADS:
            command = "grep time t%d-p%d.out | cut -d\" \" -f4" % (t, p)
            #print(command)
            process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
            output, error = process.communicate()
            #print(output.strip())
            value = output.strip()
            try:
                cell = int(float(value))
            except:
                cell = ""
            row.append(cell)
        writer.writerow(row)
print(f'CSV file "{csv_file}" has been created successfully.')
