#!/bin/bash
#SBATCH --output=slurm-dmr_%j.out

export PATH=$SLURM_ROOT/bin:$PATH
NODELIST="$(scontrol show hostname $SLURM_JOB_NODELIST | paste -d, -s)"
#echo $NODELIST

NPROCS=$((  $SLURM_JOB_NUM_NODES * 48 ))
CMD="mpirun -iface eth0 -n $NPROCS -hosts $NODELIST ./mpdata"
echo $CMD
$CMD
