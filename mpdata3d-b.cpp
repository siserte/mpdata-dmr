#include <algorithm>
#include <cmath>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <sys/time.h>
#include <sys/syscall.h>
#include <stdio.h>
#include <unistd.h>
#include <vector>

#include <omp.h>
#include <mpi.h>

#define USE_DOUBLE

#ifdef USE_DOUBLE
#define real double
#define mpi_real MPI_DOUBLE
#else
#define real float
#define mpi_real MPI_FLOAT
#endif

//mpic++ -o mpdata3d mpdata3d.cpp -W -Wall -pedantic -O3 -fopenmp
//mpirun -np 4 ./mpdata3d

const int GRID_SIZE_N=8192;
const int GRID_SIZE_M=1024;
const int GRID_SIZE_L=128;
const int TIME_STEPS=10;
const int THREADS=48;

template<typename T>
std::string toStr(const T& nr)
{
  std::ostringstream os;
  os << nr;
  return os.str();
}

template<typename T>
T strTo(const std::string &s)
{
  T num;
  std::istringstream is(s);
  is >> num;
  return num;
}

template<typename T>
T strTo(const char* s)
{
  T num;
  std::istringstream is(s);
  is >> num;
  return num;
}

class TimeCPU
{
  private:
    struct timeval ts, te;
    double t;

  public:
    TimeCPU()
    : t(0.0)
    {}

    void start()
    {
      gettimeofday(&ts, NULL);
    }

    void stop()
    {
      gettimeofday(&te, NULL);
      t = (double)(te.tv_sec-ts.tv_sec)+0.000001*(double)(te.tv_usec-ts.tv_usec);
    }

    const double& time() const { return t; }
    double& time() { return t; }
};

inline std::ostream& operator<<(std::ostream& os, const TimeCPU& t)
{
  return os << t.time();
}

int n;
int m;
int l;
int ts;
int gridSize;

int RANK;
int SIZE;
int HALO;

std::string task, workDir;

real *v1_, *v2_, *v3_,
     *f1_, *f2_, *f3_,
     *u1_, *u2_, *u3_,
     *x_, *h_,
     *ubc_, *vbc_, *wbc_,
     *bcx_, *bcy_,
     *mx_, *mn_, *cp_, *cn_;

inline int ind(int i, int j, int k)
{
  int index = (i+HALO-1)*(m+1)*(l+1) + (k-1)*(m+1) + (j-1);

  //if(index>=gridSize) std::cerr << "Out of index!\n";

  return index;
}

real& v1(int i, int j, int k) { return v1_[ ind(i, j, k) ]; }
real& v2(int i, int j, int k) { return v2_[ ind(i, j, k) ]; }
real& v3(int i, int j, int k) { return v3_[ ind(i, j, k) ]; }
real& u1(int i, int j, int k) { return u1_[ ind(i, j, k) ]; }
real& u2(int i, int j, int k) { return u2_[ ind(i, j, k) ]; }
real& u3(int i, int j, int k) { return u3_[ ind(i, j, k) ]; }
real& f1(int i, int j, int k) { return f1_[ ind(i, j, k) ]; }
real& f2(int i, int j, int k) { return f2_[ ind(i, j, k) ]; }
real& f3(int i, int j, int k) { return f3_[ ind(i, j, k) ]; }
real& x(int i, int j, int k) { return x_[ ind(i, j, k) ]; }
real& h(int i, int j, int k) { return h_[ ind(i, j, k) ]; }
real& ubc(int j, int k, int i) { return ubc_[(j-1)+(k-1)*(m+1)+(i-1)*(m+1)*(l+1)]; }
real& vbc(int i, int k, int j) { return vbc_[(i-1)+(k-1)*(n+1)+(j-1)*(n+1)*(l+1)]; }
real& wbc(int i, int j, int k) { return wbc_[(j-1)+(i-1)*(m+1)+(k-1)*(n+1)*(m+1)]; }
real& bcx(int j, int k, int i) { return bcx_[(j-1)+(k-1)*(m+1)+(i-1)*(m+1)*(l+1)]; }
real& bcy(int i, int k, int j) { return bcy_[(i-1)+(k-1)*(n+1)+(j-1)*(n+1)*(l+1)]; }
real& mx(int i, int j, int k) { return mx_[ ind(i, j, k) ]; }
real& mn(int i, int j, int k) { return mn_[ ind(i, j, k) ]; }
real& cp(int i, int j, int k) { return cp_[ ind(i, j, k) ]; }
real& cn(int i, int j, int k) { return cn_[ ind(i, j, k) ]; }

real myMax(real a0, real a1, real a2)
{
  a0 = std::max(a0, a1);
  return std::max(a0, a2);
}

real myMin(real a0, real a1, real a2)
{
  a0 = std::min(a0, a1);
  return std::min(a0, a2);
}

real myMax(real a0, real a1, real a2, real a3, real a4, real a5, real a6)
{
  a0 = std::max(a0, a1);
  a2 = std::max(a2, a3);
  a4 = std::max(a4, a5);
  a0 = std::max(a0, a6);
  a0 = std::max(a0, a2);
  return std::max(a0, a4);
}

real myMin(real a0, real a1, real a2, real a3, real a4, real a5, real a6)
{
  a0 = std::min(a0, a1);
  a2 = std::min(a2, a3);
  a4 = std::min(a4, a5);
  a0 = std::min(a0, a6);
  a0 = std::min(a0, a2);
  return std::min(a0, a4);
}

real myMax(real a0, real a1, real a2, real a3, real a4, real a5, real a6, real a7)
{
  a0 = std::max(a0, a1);
  a2 = std::max(a2, a3);
  a4 = std::max(a4, a5);
  a0 = std::max(a0, a6);
  a0 = std::max(a0, a2);
  a0 = std::max(a0, a7);
  return std::max(a0, a4);
}

real myMin(real a0, real a1, real a2, real a3, real a4, real a5, real a6, real a7)
{
  a0 = std::min(a0, a1);
  a2 = std::min(a2, a3);
  a4 = std::min(a4, a5);
  a0 = std::min(a0, a6);
  a0 = std::min(a0, a2);
  a0 = std::min(a0, a7);
  return std::min(a0, a4);
}

int init(int argc, char** argv)
{
  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &RANK);
  MPI_Comm_size(MPI_COMM_WORLD, &SIZE);
  HALO=3;
  
  task="";
  workDir="";

  int err=0;

  n = GRID_SIZE_N/SIZE;
  m = GRID_SIZE_M;
  l = GRID_SIZE_L;
  ts = TIME_STEPS;

  gridSize = (n+2*HALO)*(m+1)*(l+1);

  v1_ = new real[gridSize];
  v2_ = new real[gridSize];
  v3_ = new real[gridSize];
  u1_ = new real[gridSize];// input / output
  u2_ = new real[gridSize];// input / output
  u3_ = new real[gridSize];// input / output
  f1_ = new real[gridSize];
  f2_ = new real[gridSize];
  f3_ = new real[gridSize];
  x_ = new real[gridSize];// input / output
  h_ = new real[gridSize];// input / output
  ubc_ = new real[gridSize];
  vbc_ = new real[gridSize];
  wbc_ = new real[gridSize];
  bcx_ = new real[gridSize];
  bcy_ = new real[gridSize];
  mx_ = new real[gridSize];
  mn_ = new real[gridSize];
  cp_ = new real[gridSize];
  cn_ = new real[gridSize];

//  fin.read((char*)buffer, n*m*l*sizeof(real)); //u1
  int index;
  for(int i=1; i<=n; ++i)
    for(int j=1; j<=m; ++j)
      for(int k=1; k<=l; ++k)
      {
        u1(i, j, k) = i;
      }

//  fin.read((char*)buffer, n*m*l*sizeof(real)); //u2
  for(int i=1; i<=n; ++i)
    for(int j=1; j<=m; ++j)
      for(int k=1; k<=l; ++k)
      {
        u2(i, j, k) = j;
      }

//  fin.read((char*)buffer, n*m*l*sizeof(real)); //u3
  for(int i=1; i<=n; ++i)
    for(int j=1; j<=m; ++j)
      for(int k=1; k<=l; ++k)
      {
        u3(i, j, k) = k;
      }

//  fin.read((char*)buffer, n*m*l*sizeof(real)); //x
  for(int i=1; i<=n; ++i)
    for(int j=1; j<=m; ++j)
      for(int k=1; k<=l; ++k)
      {
        index = (i-1)*m*l + (k-1)*m + (j-1);
        x(i, j, k) = index;
      }

//  fin.read((char*)buffer, n*m*l*sizeof(real)); //h
  for(int i=1; i<=n; ++i)
    for(int j=1; j<=m; ++j)
      for(int k=1; k<=l; ++k)
      {
        h(i, j, k) = 1.0;
      }

//  fin.read((char*)buffer, m*l*2*sizeof(real)); //ubc
  for(int i=1; i<=2; ++i)
    for(int j=1; j<=m; ++j)
      for(int k=1; k<=l; ++k)
      {
        ubc(j, k, i) = i+j;
      }

//  fin.read((char*)buffer, n*l*2*sizeof(real)); //vbc
  for(int i=1; i<=n; ++i)
    for(int j=1; j<=2; ++j)
      for(int k=1; k<=l; ++k)
      {
        vbc(i, k, j) = i+k;
      }

//  fin.read((char*)buffer, n*m*2*sizeof(real)); //wbc
  for(int i=1; i<=n; ++i)
    for(int j=1; j<=m; ++j)
      for(int k=1; k<=2; ++k)
      {
        wbc(i, j, k) = j+k;
      }

//  fin.read((char*)buffer, m*l*2*sizeof(real)); //bcx
  for(int i=1; i<=2; ++i)
    for(int j=1; j<=m; ++j)
      for(int k=1; k<=l; ++k)
      {
        bcx(j, k, i) = i+j+k;
      }

//  fin.read((char*)buffer, l*n*2*sizeof(real)); //bcy
  for(int i=1; i<=n; ++i)
    for(int j=1; j<=2; ++j)
      for(int k=1; k<=l; ++k)
      {
        bcy(i, k, j) = 2;
      }

//  fin.close();

  for(int i=1; i<=n; ++i)
    for(int j=1; j<=m; ++j)
      for(int k=1; k<=l; ++k)
      {
        v1(i, j, k) = 0.0;
        v2(i, j, k) = 0.0;
        v3(i, j, k) = 0.0;
        f1(i, j, k) = 0.0;
        f2(i, j, k) = 0.0;
        f3(i, j, k) = 0.0;
        mx(i, j, k) = 0.0;
        mn(i, j, k) = 0.0;
        cp(i, j, k) = 0.0;
        cn(i, j, k) = 0.0;
      }

  return err;
}

void release()
{
  delete [] v1_;
  delete [] v2_;
  delete [] v3_;
  delete [] u1_;
  delete [] u2_;
  delete [] u3_;
  delete [] f1_;
  delete [] f2_;
  delete [] f3_;
  delete [] x_;
  delete [] h_;
  delete [] ubc_;
  delete [] vbc_;
  delete [] wbc_;
  delete [] bcx_;
  delete [] bcy_;
  delete [] mx_;
  delete [] mn_;
  delete [] cp_;
  delete [] cn_;
  
  MPI_Finalize();
}

/*
void update()
{
  const unsigned int rank = RANK;
  const unsigned int size = SIZE;
  const unsigned int haloSize = HALO*(m+1)*(l+1);
  const unsigned int offsetS = gridSize-2*haloSize;
  const unsigned int offsetR = gridSize-haloSize;
    
  MPI_Request requests[16];
  MPI_Status statuses[16];
  
  if(rank < size - 1)
  {
    MPI_Isend(&x_[offsetS], haloSize, mpi_real, rank + 1, 10, MPI_COMM_WORLD, &requests[0]);
    MPI_Irecv(&x_[offsetR], haloSize, mpi_real, rank + 1, 11, MPI_COMM_WORLD, &requests[1]);

    MPI_Isend(&u1_[offsetS], haloSize, mpi_real, rank + 1, 20, MPI_COMM_WORLD, &requests[2]);
    MPI_Irecv(&u1_[offsetR], haloSize, mpi_real, rank + 1, 21, MPI_COMM_WORLD, &requests[3]);

    MPI_Isend(&u2_[offsetS], haloSize, mpi_real, rank + 1, 30, MPI_COMM_WORLD, &requests[4]);
    MPI_Irecv(&u2_[offsetR], haloSize, mpi_real, rank + 1, 31, MPI_COMM_WORLD, &requests[5]);

    MPI_Isend(&u3_[offsetS], haloSize, mpi_real, rank + 1, 40, MPI_COMM_WORLD, &requests[6]);
    MPI_Irecv(&u3_[offsetR], haloSize, mpi_real, rank + 1, 41, MPI_COMM_WORLD, &requests[7]);
  }
  
  if(rank > 0)
  {
    MPI_Irecv(&x_[0], haloSize, mpi_real, rank - 1, 10, MPI_COMM_WORLD, &requests[8]);
    MPI_Isend(&x_[haloSize], haloSize, mpi_real, rank - 1, 11, MPI_COMM_WORLD, &requests[9]);
    
    MPI_Irecv(&u1_[0], haloSize, mpi_real, rank - 1, 20, MPI_COMM_WORLD, &requests[10]);
    MPI_Isend(&u1_[haloSize], haloSize, mpi_real, rank - 1, 21, MPI_COMM_WORLD, &requests[11]);
    
    MPI_Irecv(&u2_[0], haloSize, mpi_real, rank - 1, 30, MPI_COMM_WORLD, &requests[12]);
    MPI_Isend(&u2_[haloSize], haloSize, mpi_real, rank - 1, 31, MPI_COMM_WORLD, &requests[13]);
    
    MPI_Irecv(&u3_[0], haloSize, mpi_real, rank - 1, 40, MPI_COMM_WORLD, &requests[14]);
    MPI_Isend(&u3_[haloSize], haloSize, mpi_real, rank - 1, 41, MPI_COMM_WORLD, &requests[15]);
  }

  if(rank < size - 1)
  {
    MPI_Wait(&requests[0], &statuses[0]);
    MPI_Wait(&requests[2], &statuses[1]);
    MPI_Wait(&requests[4], &statuses[2]);
    MPI_Wait(&requests[6], &statuses[3]);
    MPI_Wait(&requests[1], &statuses[4]);
    MPI_Wait(&requests[3], &statuses[5]);
    MPI_Wait(&requests[5], &statuses[6]);
    MPI_Wait(&requests[7], &statuses[7]);
  }
  
  if(rank > 0)
  {
    MPI_Wait(&requests[8], &statuses[8]);
    MPI_Wait(&requests[10], &statuses[9]);
    MPI_Wait(&requests[12], &statuses[10]);
    MPI_Wait(&requests[14], &statuses[11]);
    MPI_Wait(&requests[9], &statuses[12]);
    MPI_Wait(&requests[11], &statuses[13]);
    MPI_Wait(&requests[13], &statuses[14]);
    MPI_Wait(&requests[15], &statuses[15]);
  }
}
*/

void update()
{
  const unsigned int rank = RANK;
  const unsigned int size = SIZE;
  const unsigned int haloSize = HALO*(m+1)*(l+1);
  const unsigned int offsetS = gridSize-2*haloSize;
  const unsigned int offsetR = gridSize-haloSize;
  
  MPI_Status statuses[2];
  
  if(rank < size - 1)
  {
    MPI_Send(&x_[offsetS], haloSize, mpi_real, rank + 1, 10, MPI_COMM_WORLD);
    MPI_Recv(&x_[0], haloSize, mpi_real, rank + 1, 11, MPI_COMM_WORLD, &statuses[0]);
    
    MPI_Send(&u1_[offsetS], haloSize, mpi_real, rank + 1, 10, MPI_COMM_WORLD);
    MPI_Recv(&u1_[0], haloSize, mpi_real, rank + 1, 11, MPI_COMM_WORLD, &statuses[0]);
    
    MPI_Send(&u2_[offsetS], haloSize, mpi_real, rank + 1, 10, MPI_COMM_WORLD);
    MPI_Recv(&u2_[0], haloSize, mpi_real, rank + 1, 11, MPI_COMM_WORLD, &statuses[0]);
    
    MPI_Send(&u3_[offsetS], haloSize, mpi_real, rank + 1, 10, MPI_COMM_WORLD);
    MPI_Recv(&u3_[0], haloSize, mpi_real, rank + 1, 11, MPI_COMM_WORLD, &statuses[0]);
  }
  if(rank > 0)
  {
    MPI_Recv(&x_[offsetR], haloSize, mpi_real, rank - 1, 10, MPI_COMM_WORLD, &statuses[1]);
    MPI_Send(&x_[haloSize], haloSize, mpi_real, rank - 1, 11, MPI_COMM_WORLD);
    
    MPI_Recv(&u1_[offsetR], haloSize, mpi_real, rank - 1, 10, MPI_COMM_WORLD, &statuses[1]);
    MPI_Send(&u1_[haloSize], haloSize, mpi_real, rank - 1, 11, MPI_COMM_WORLD);
    
    MPI_Recv(&u2_[offsetR], haloSize, mpi_real, rank - 1, 10, MPI_COMM_WORLD, &statuses[1]);
    MPI_Send(&u2_[haloSize], haloSize, mpi_real, rank - 1, 11, MPI_COMM_WORLD);
    
    MPI_Recv(&u3_[offsetR], haloSize, mpi_real, rank - 1, 10, MPI_COMM_WORLD, &statuses[1]);
    MPI_Send(&u3_[haloSize], haloSize, mpi_real, rank - 1, 11, MPI_COMM_WORLD);
  }
}

real pp(real y) { return  std::max(real(0.0), y); }
real pn(real y) { return -std::min(real(0.0), y); }
real donor(real y1, real y2, real a) { return pp(a)*y1-pn(a)*y2; }
real rat2(real z1, real z2) { return (z2-z1)*.5; }
real rat4(real z0, real z1, real z2, real z3) { return (z3+z2-z1-z0)*.25; }
real vdyf(real x1, real x2, real a, real r) { return (fabs(a)-a*a/r)*rat2(x1,x2); }
real vcorr(real a, real b, real y0, real y1, real y2, real y3, real r)
{
  return -0.125*a*b/r*rat4(y0,y1,y2,y3);
}

//--- MPDATA 3D: begin ---

TimeCPU t;

void mpdata3d()
{
      int leftedge=1;
      int rightedge=1;
      int botedge=(RANK==SIZE-1);
      int topedge=(RANK==0);

//      int liner=1;

      int np=n;
      int mp=m;

//      int n1=n+1;
//      int n2=m+1;
      int n3=l+1;
//      int n1m=n1-1;
//      int n2m=n2-1;
      int n3m=n3-1;

      int ibcx=0;
      int ibcy=0;
      int ibcz=0;

      int ibox=1-ibcx;
      int iboy=1-ibcy;
      int iboz=1-ibcz;
//      int ibcxy=ibcx*ibcy;
//      int itmx=2-liner;

      int iprec=0;

      int nonos=1;

      real ep=1.e-10;

    for(int TS=0; TS<ts; ++TS) // main loop
    {
      #pragma omp single
      {
        update();
        if(RANK==0) std::cout << "Timestep " << TS << " is updated\n";
      }
      
      for(int k=2; k<=n3m; ++k)
        for(int j=1; j<=mp; ++j)
          #pragma omp for
          for(int i=1; i<=np; ++i)
            v3(i,j,k) = u3(i,j,k);
            
      for(int j=1; j<=mp; ++j)
        #pragma omp for
        for(int i=1; i<=np; ++i)
        {
          v3(i,j, 1) = wbc(i,j,1);
          v3(i,j,n3) = wbc(i,j,2);
        }

      int illim = 1 + 1*leftedge;

      for(int k=1; k<=n3m; ++k)
        for(int j=1; j<=mp; ++j)
          #pragma omp for
          for(int i=illim; i<=np; ++i)
            v1(i,j,k) = u1(i,j,k);

      if (leftedge==1)
      {
        #pragma omp for
        for(int k=1; k<=n3m; ++k)
          for(int j=1; j<=mp; ++j)
            v1( 1,j,k) = ubc(j,k,1);
      }
      if (rightedge==1)
      {
        #pragma omp for
        for(int k=1; k<=n3m; ++k)
          for(int j=1; j<=mp; ++j)
            v1(np+1,j,k) = ubc(j,k,2);
      }

      int jllim = 1 + 1*botedge;

      for(int k=1; k<=n3m; ++k)
        for(int j=jllim; j<=mp; ++j)
          #pragma omp for
          for(int i=1; i<=np; ++i)
            v2(i,j,k) = u2(i,j,k);

      if (botedge==1)
      {
        for(int k=1; k<=n3m; ++k)
          #pragma omp for
          for(int i=1; i<=np; ++i)
            v2(i, 1,k) = vbc(i,k,1);
      }
      if (topedge==1)
      {
        for(int k=1; k<=n3m; ++k)
          #pragma omp for
          for(int i=1; i<=np; ++i)
            v2(i,mp+1,k) = vbc(i,k,2);
      }

      if (nonos==1)
      {
        int jm, jp, im, ip;
        for(int k=1; k<=n3m; ++k)
        {
          int km=ibcz*(k-1+(n3-k)/n3m*(n3-2))+(1-ibcz)*std::max(k-1,1);
          int kp=ibcz*(k+1    -k /n3m*(n3-2))+(1-ibcz)*std::min(k+1,n3m);
          for(int j=1; j<=mp; ++j)
          {
            if (botedge==1 && j==1)
            {
              jm = ibcy*(-1) + iboy*1;
            } else {
              jm = j - 1;
            }
            if (topedge==1 && j==mp)
            {
              jp = ibcy*(mp+2) + iboy*mp;
            } else {
              jp = j + 1;
            }
            #pragma omp for
            for(int i=1; i<=np; ++i)
            {
              if (leftedge==1 && i==1)
              {
                im = ibcx*(-1) + ibox*1;
              } else {
                im = i - 1;
              }
              if (rightedge==1 && i==np)
              {
                ip = ibcx*(np+2) + ibox*np;
              } else {
                ip = i + 1;
              }
              mx(i,j,k)=myMax(x(im,j,k),x(i,j,k),x(ip,j,k),
                              x(i,jm,k),x(i,jp,k),x(i,j,kp),x(i,j,km));
              mn(i,j,k)=myMin(x(im,j,k),x(i,j,k),x(ip,j,k),
                              x(i,jm,k),x(i,jp,k),x(i,j,kp),x(i,j,km));
            }
          }
        }
      }

      real c1=1.;
      real c2=0.;

      int ilft=1+leftedge*1;
      for(int k=1; k<=n3m; ++k)
        for(int j=1; j<=mp; ++j)
          #pragma omp for
          for(int i=ilft; i<=np; ++i)
            f1(i,j,k)=donor(c1*x(i-1,j,k)+c2,c1*x(i,j,k)+c2,v1(i,j,k));

      if(ibcx==1)
      {
        if (leftedge==1)
        {
        #pragma omp for
          for(int k=1; k<=n3m; ++k)
            for(int j=1; j<=mp; ++j)
              f1(1 ,j,k)=f1(-1,j,k);
        }
        if (rightedge==1)
        {
        #pragma omp for
          for(int k=1; k<=n3m; ++k)
            for(int j=1; j<=mp; ++j)
              f1(np+1,j,k)=f1(np+3,j,k);
        }
      } else {
        if (leftedge==1)
        {
        #pragma omp for
          for(int k=1; k<=n3m; ++k)
            for(int j=1; j<=mp; ++j)
              f1(1 ,j,k)=donor(c1*bcx(j,k,1)+c2,c1*x(1,j,k)+c2,v1(1,j,k));
        }
        if (rightedge==1)
        {
        #pragma omp for
          for(int k=1; k<=n3m; ++k)
            for(int j=1; j<=mp; ++j)
              f1(np+1,j,k)=donor(c1*x(np,j,k)+c2,c1*bcx(j,k,2)+c2,v1(np+1,j,k));
        }
      }
      
      int jbot=1+botedge*1;
      for(int k=1; k<=n3m; ++k)
        for(int j=jbot; j<=mp; ++j)
        #pragma omp for
          for(int i=1; i<=np; ++i)
            f2(i,j,k)=donor(c1*x(i,j-1,k)+c2,c1*x(i,j,k)+c2,v2(i,j,k));

      if(ibcy==1)
      {
        if (botedge==1)
        {
          for(int k=1; k<=n3m; ++k)
          #pragma omp for
            for(int i=1; i<=np; ++i)
              f2(i,1,k)=f2(i,-1,k);
        }
        if (topedge==1)
        {
          for(int k=1; k<=n3m; ++k)
          #pragma omp for
            for(int i=1; i<=np; ++i)
              f2(i,mp+1,k)=f2(i,mp+3,k);
        }
      } else {
        if (botedge==1)
        {
          for(int k=1; k<=n3m; ++k)
          #pragma omp for
            for(int i=1; i<=np; ++i)
              f2(i,1,k)=donor(c1*bcy(i,k,1)+c2,c1*x(i,1,k)+c2,v2(i,1,k));
        }
        if (topedge==1)
        {
          for(int k=1; k<=n3m; ++k)
          #pragma omp for
            for(int i=1; i<=np; ++i)
              f2(i,mp+1,k)=donor(c1*x(i,mp,k)+c2,c1*bcy(i,k,2)+c2,v2(i,mp+1,k));
        }
      }
  
      for(int k=2; k<=n3m; ++k)
        for(int j=1; j<=mp; ++j)
        #pragma omp for
          for(int i=1; i<=np; ++i)
            f3(i,j,k)=donor(c1*x(i,j,k-1)+c2,c1*x(i,j,k)+c2,v3(i,j,k));

      if (iprec==1)
      {
        for(int j=1; j<=mp; ++j)
        #pragma omp for
          for(int i=1; i<=np; ++i)
          {
            f3(i,j, 1)=donor(c1*x(i,j, 1 )+c2,c1*x(i,j, 1 )+c2,v3(i,j,1 ));
            f3(i,j,n3)=donor(c1*x(i,j,n3m)+c2,c1*x(i,j,n3m)+c2,v3(i,j,n3));
          }
      } else {
        for(int j=1; j<=mp; ++j)
        #pragma omp for
          for(int i=1; i<=np; ++i)
          {
            f3(i,j, 1)=-f3(i,j,  2)*iboz+f3(i,j,n3m)*ibcz;
            f3(i,j,n3)=-f3(i,j,n3m)*iboz+f3(i,j, 2 )*ibcz;
          }
      }
  
      for(int k=1; k<=n3m; ++k)
        for(int j=1; j<=mp; ++j)
        #pragma omp for
          for(int i=1; i<=np; ++i)
            x(i,j,k)=x(i,j,k)-( f1(i+1,j,k)-f1(i,j,k)
                               +f2(i,j+1,k)-f2(i,j,k)
                               +f3(i,j,k+1)-f3(i,j,k) )/h(i,j,k);

      c1=0.;
      c2=1.;

      int iulim = np + 1*rightedge;
      int julim = mp + 1*topedge;

      for(int k=1; k<=n3m; ++k)
        for(int j=1; j<=mp; ++j)
        #pragma omp for
          for(int i=1; i<=iulim; ++i)
          {
            f1(i,j,k)=v1(i,j,k);
            v1(i,j,k)=0.;
          }

      for(int k=1; k<=n3m; ++k)
        for(int j=1; j<=julim; ++j)
        #pragma omp for
          for(int i=1; i<=np; ++i)
          {
            f2(i,j,k)=v2(i,j,k);
            v2(i,j,k)=0.;
          }

      for(int k=1; k<=n3; ++k)
        for(int j=1; j<=mp; ++j)
        #pragma omp for
          for(int i=1; i<=np; ++i)
          {
            f3(i,j,k)=v3(i,j,k);
            v3(i,j,k)=0.;
          }

//------------
//compute antidiffusive velocities in x direction
//------------

      illim = 1 + 1*leftedge;
      iulim = np;
      jllim = 1  + 1*botedge;
      julim = mp - 1*topedge;

      for(int k=2; k<=n3-2; ++k)
        for(int j=jllim; j<=julim; ++j)
        #pragma omp for
          for(int i=illim; i<=iulim; ++i)
          {
            v1(i,j,k)=vdyf(x(i-1,j,k),x(i,j,k),f1(i,j,k),
                      .5*(h(i-1,j,k)+h(i,j,k)))
            +vcorr(f1(i,j,k),
            f2(i-1,j,k)+f2(i-1,j+1,k)+f2(i,j+1,k)+f2(i,j,k),
            x(i-1,j-1,k),x(i,j-1,k),x(i-1,j+1,k),x(i,j+1,k),
                      .5*(h(i-1,j,k)+h(i,j,k)))
            +vcorr(f1(i,j,k),
            f3(i-1,j,k)+f3(i-1,j,k+1)+f3(i,j,k+1)+f3(i,j,k),
            x(i-1,j,k-1),x(i,j,k-1),x(i-1,j,k+1),x(i,j,k+1),
                      .5*(h(i-1,j,k)+h(i,j,k)));
          }

//------------
//compute antidiffusive velocities in y direction
//------------

         illim = 1  + 1*leftedge;
         iulim = np - 1*rightedge;
         jllim = 1 + 1*botedge;
         julim = mp;

      for(int k=2; k<=n3-2; ++k)
        for(int j=jllim; j<=julim; ++j)
        #pragma omp for
          for(int i=illim; i<=iulim; ++i)
          {
            v2(i,j,k)=vdyf(x(i,j-1,k),x(i,j,k),f2(i,j,k),
                      .5*(h(i,j-1,k)+h(i,j,k)))
            +vcorr(f2(i,j,k),
            f1(i,j-1,k)+f1(i,j,k)+f1(i+1,j,k)+f1(i+1,j-1,k),
            x(i-1,j-1,k),x(i-1,j,k),x(i+1,j-1,k),x(i+1,j,k),
                      .5*(h(i,j-1,k)+h(i,j,k)))
            +vcorr(f2(i,j,k),
            f3(i,j-1,k)+f3(i,j,k)+f3(i,j,k+1)+f3(i,j-1,k+1),
            x(i,j-1,k-1),x(i,j,k-1),x(i,j-1,k+1),x(i,j,k+1),
                      .5*(h(i,j-1,k)+h(i,j,k)));
          }

//------------
//compute antidiffusive velocities in z direction
//------------

      illim = 1  + 1*leftedge;
      iulim = np - 1*rightedge;
      jllim = 1  + 1*botedge;
      julim = mp - 1*topedge;

      for(int k=2; k<=n3m; ++k)
      for(int j=jllim; j<=julim; ++j)
      #pragma omp for
      for(int i=illim; i<=iulim; ++i)
      {
        v3(i,j,k)=vdyf(x(i,j,k-1),x(i,j,k),f3(i,j,k),
                      .5*(h(i,j,k-1)+h(i,j,k)))
        +vcorr(f3(i,j,k),
           f1(i,j,k-1)+f1(i,j,k)+f1(i+1,j,k)+f1(i+1,j,k-1),
           x(i-1,j,k-1),x(i-1,j,k),x(i+1,j,k-1),x(i+1,j,k),
                      .5*(h(i,j,k-1)+h(i,j,k)))
        +vcorr(f3(i,j,k),
           f2(i,j,k-1)+f2(i,j+1,k-1)+f2(i,j+1,k)+f2(i,j,k),
           x(i,j-1,k-1),x(i,j-1,k),x(i,j+1,k-1),x(i,j+1,k),
                      .5*(h(i,j,k-1)+h(i,j,k)));
      }

      if (iprec==0)
      {
        for(int j=1; j<=mp; ++j)
        #pragma omp for
          for(int i=1; i<=np; ++i)
          {
            v3(i,j, 1)=-v3(i,j,  2)*iboz+v3(i,j,n3m)*ibcz;
            v3(i,j,n3)=-v3(i,j,n3m)*iboz+v3(i,j, 2 )*ibcz;
          }
      }

                  if(nonos==1)
                  {
        int jm, jp, im, ip;
        for(int k=1; k<=n3m; ++k)
        {
          int km=ibcz*(k-1+(n3-k)/n3m*(n3-2))+(1-ibcz)*std::max(k-1,1);
          int kp=ibcz*(k+1    -k /n3m*(n3-2))+(1-ibcz)*std::min(k+1,n3m);
          for(int j=1; j<=mp; ++j)
          {
            if (botedge==1 && j==1)
            {
              jm = ibcy*(-1) + iboy*1;
            } else {
              jm = j - 1;
            }
            if (topedge==1 && j==mp)
            {
              jp = ibcy*(mp+2) + iboy*mp;
            } else {
              jp = j + 1;
            }
            #pragma omp for
            for(int i=1; i<=np; ++i)
            {
              if (leftedge==1 && i==1)
              {
                im = ibcx*(-1) + ibox*1;
              } else {
                im = i - 1;
              }
              if (rightedge==1 && i==np)
              {
                ip = ibcx*(np+2) + ibox*np;
              } else {
                ip = i + 1;
              }
              mx(i,j,k)=myMax(x(im,j,k),x(i,j,k),x(ip,j,k),mx(i,j,k),
                              x(i,jm,k),x(i,jp,k),x(i,j,kp),x(i,j,km));
              mn(i,j,k)=myMin(x(im,j,k),x(i,j,k),x(ip,j,k),mn(i,j,k),
                              x(i,jm,k),x(i,jp,k),x(i,j,kp),x(i,j,km));
            }
          }
        }

      iulim = np + 1*rightedge;
      julim = mp + 1*topedge;

      for(int k=1; k<=n3m; ++k)
      for(int j=1; j<=mp; ++j)
      #pragma omp for
      for(int i=1; i<=iulim; ++i)
      f1(i,j,k)=donor(c2,c2,v1(i,j,k));

      for(int k=1; k<=n3m; ++k)
      for(int j=1; j<=julim; ++j)
      #pragma omp for
      for(int i=1; i<=np; ++i)
      f2(i,j,k)=donor(c2,c2,v2(i,j,k));

      for(int k=1; k<=n3; ++k)
      for(int j=1; j<=mp; ++j)
      #pragma omp for
      for(int i=1; i<=np; ++i)
      f3(i,j,k)=donor(c2,c2,v3(i,j,k));

      for(int k=1; k<=n3m; ++k)
      for(int j=1; j<=mp; ++j)
      #pragma omp for
      for(int i=1; i<=np; ++i)
      {
        cp(i,j,k)=(mx(i,j,k)-x(i,j,k))*h(i,j,k)/
        ( pn(f1(i+1,j,k))+pp(f1(i,j,k))
         +pn(f2(i,j+1,k))+pp(f2(i,j,k))
         +pn(f3(i,j,k+1))+pp(f3(i,j,k))+ep);

        cn(i,j,k)=(x(i,j,k)-mn(i,j,k))*h(i,j,k)/
        ( pp(f1(i+1,j,k))+pn(f1(i,j,k))
         +pp(f2(i,j+1,k))+pn(f2(i,j,k))
         +pp(f3(i,j,k+1))+pn(f3(i,j,k))+ep);
      }

      illim = 1 + 1*leftedge;
      jllim = 1 + 1*botedge;

      for(int k=1; k<=n3m; ++k)
        for(int j=1; j<=mp; ++j)
        #pragma omp for
          for(int i=illim; i<=np; ++i)
            v1(i,j,k)= pp(v1(i,j,k))*myMin(1.,cp(i,j,k),cn(i-1,j,k))
                      -pn(v1(i,j,k))*myMin(1.,cp(i-1,j,k),cn(i,j,k));

      for(int k=1; k<=n3m; ++k)
        for(int j=jllim; j<=mp; ++j)
        #pragma omp for
          for(int i=1; i<=np; ++i)
            v2(i,j,k)= pp(v2(i,j,k))*myMin(1.,cp(i,j,k),cn(i,j-1,k))
                      -pn(v2(i,j,k))*myMin(1.,cp(i,j-1,k),cn(i,j,k));

      for(int k=2; k<=n3m; ++k)
        for(int j=1; j<=mp; ++j)
        #pragma omp for
          for(int i=1; i<=np; ++i)
            v3(i,j,k)= pp(v3(i,j,k))*myMin(1.,cp(i,j,k),cn(i,j,k-1))
                      -pn(v3(i,j,k))*myMin(1.,cp(i,j,k-1),cn(i,j,k));

      if (iprec==0)
      {
        for(int j=1; j<=mp; ++j)
        #pragma omp for
          for(int i=1; i<=np; ++i)
          {
            v3(i,j, 1)=-v3(i,j,  2)*iboz+v3(i,j,n3m)*ibcz;
            v3(i,j,n3)=-v3(i,j,n3m)*iboz+v3(i,j, 2 )*ibcz;
          }
      }
                  }

      ilft=1+leftedge*1;
      for(int k=1; k<=n3m; ++k)
        for(int j=1; j<=mp; ++j)
        #pragma omp for
          for(int i=ilft; i<=np; ++i)
            f1(i,j,k)=donor(c1*x(i-1,j,k)+c2,c1*x(i,j,k)+c2,v1(i,j,k));

      if(ibcx==1)
      {
        if (leftedge==1)
        {
        #pragma omp for
          for(int k=1; k<=n3m; ++k)
            for(int j=1; j<=mp; ++j)
              f1(1 ,j,k)=f1(-1,j,k);
        }
        if (rightedge==1)
        {
        #pragma omp for
          for(int k=1; k<=n3m; ++k)
            for(int j=1; j<=mp; ++j)
              f1(np+1,j,k)=f1(np+3,j,k);
        }
      } else {
        if (leftedge==1)
        {
        #pragma omp for
          for(int k=1; k<=n3m; ++k)
            for(int j=1; j<=mp; ++j)
              f1(1 ,j,k)=donor(c1*bcx(j,k,1)+c2,c1*x(1,j,k)+c2,v1(1,j,k));
        }
        if (rightedge==1)
        {
        #pragma omp for
          for(int k=1; k<=n3m; ++k)
            for(int j=1; j<=mp; ++j)
              f1(np+1,j,k)=donor(c1*x(np,j,k)+c2,c1*bcx(j,k,2)+c2,v1(np+1,j,k));
        }
      }

      jbot=1+botedge*1;
      for(int k=1; k<=n3m; ++k)
        for(int j=jbot; j<=mp; ++j)
        #pragma omp for
          for(int i=1; i<=np; ++i)
            f2(i,j,k)=donor(c1*x(i,j-1,k)+c2,c1*x(i,j,k)+c2,v2(i,j,k));

      if(ibcy==1)
      {
        if (botedge==1)
        {
          for(int k=1; k<=n3m; ++k)
          #pragma omp for
            for(int i=1; i<=np; ++i)
              f2(i,1,k)=f2(i,-1,k);
        }
        if (topedge==1)
        {
          for(int k=1; k<=n3m; ++k)
          #pragma omp for
            for(int i=1; i<=np; ++i)
              f2(i,mp+1,k)=f2(i,mp+3,k);
        }
      } else {
        if (botedge==1)
        {
          for(int k=1; k<=n3m; ++k)
          #pragma omp for
            for(int i=1; i<=np; ++i)
              f2(i,1,k)=donor(c1*bcy(i,k,1)+c2,c1*x(i,1,k)+c2,v2(i,1,k));
        }
        if (topedge==1)
        {
          for(int k=1; k<=n3m; ++k)
          #pragma omp for
            for(int i=1; i<=np; ++i)
              f2(i,mp+1,k)=donor(c1*x(i,mp,k)+c2,c1*bcy(i,k,2)+c2,v2(i,mp+1,k));
        }
      }

      for(int k=2; k<=n3m; ++k)
        for(int j=1; j<=mp; ++j)
        #pragma omp for
          for(int i=1; i<=np; ++i)
            f3(i,j,k)=donor(c1*x(i,j,k-1)+c2,c1*x(i,j,k)+c2,v3(i,j,k));

      if (iprec==1)
      {
        for(int j=1; j<=mp; ++j)
        #pragma omp for
          for(int i=1; i<=np; ++i)
          {
            f3(i,j, 1)=donor(c1*x(i,j, 1 )+c2,c1*x(i,j, 1 )+c2,v3(i,j,1 ));
            f3(i,j,n3)=donor(c1*x(i,j,n3m)+c2,c1*x(i,j,n3m)+c2,v3(i,j,n3));
          }
      } else {
        for(int j=1; j<=mp; ++j)
        #pragma omp for
          for(int i=1; i<=np; ++i)
          {
            f3(i,j, 1)=-f3(i,j,  2)*iboz+f3(i,j,n3m)*ibcz;
            f3(i,j,n3)=-f3(i,j,n3m)*iboz+f3(i,j, 2 )*ibcz;
          }
      }

      for(int k=1; k<=n3m; ++k)
        for(int j=1; j<=mp; ++j)
        #pragma omp for
          for(int i=1; i<=np; ++i)
            x(i,j,k)=x(i,j,k)-( f1(i+1,j,k)-f1(i,j,k)
                               +f2(i,j+1,k)-f2(i,j,k)
                               +f3(i,j,k+1)-f3(i,j,k) )/h(i,j,k);
    }
}
//--- MPDATA 3D: end ---

int main(int argc, char** argv)
{
  if( init(argc, argv) )
  {
    std::cerr << "Error while initializing!\n";
    return -1;
  }

  if( RANK==0 )
  {
    std::cout << "Threads:      " << THREADS << '\n';
    std::cout << "MPI tasks:    " << SIZE << '\n';
    std::cout << "Grid size:    "
              << GRID_SIZE_N<<'x'<<GRID_SIZE_M<<'x'<<GRID_SIZE_L << '\n';
    std::cout << "Time steps:   " << TIME_STEPS << '\n';
  }
  
  omp_set_num_threads(THREADS);

  TimeCPU t1;
  t1.start();

#pragma omp parallel
{
  //Thread affinity
  cpu_set_t set;
  CPU_ZERO(&set);
  CPU_SET(omp_get_thread_num(), &set);
  pid_t tid = (pid_t)syscall(SYS_gettid);
  sched_setaffinity(tid, sizeof(set), &set);
  
  mpdata3d();
}

  t1.stop();
  
  if(RANK==0) std::cout << "Execution time is: " << t1 << '\n';

  release();

  return 0;
}
