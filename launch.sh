#!/bin/bash
#SBATCH --output=slurm-dmr_%j.out
#SBATCH --mail-type=END

export PATH=$SLURM_ROOT/bin:$PATH
NODELIST="$(scontrol show hostname $SLURM_JOB_NODELIST | paste -d, -s)"
echo $NODELIST

mpirun -iface eth0 -n $SLURM_JOB_NUM_NODES -hosts $NODELIST ./mpdata
